import { CartItemInterface } from './reducer';
import { ApplicationState } from "..";

const cartStateSelector = (state: ApplicationState) => state.cartReducer;

export const cartItemsStateSelector = (state: ApplicationState): CartItemInterface[] => cartStateSelector(state) && cartStateSelector(state).cart;

export const cartSummaryItemsValueSelector = (state: ApplicationState): number => {
  const list = state.cartReducer.cart && state.cartReducer.cart.length && state.cartReducer.cart;
  if (list.length >= 2) {
    const values: number[] = list.map(item => item.value);
    return values.reduce((a, b): number => {
      return parseFloat((a + b).toFixed(2));
    }, 0)
  }
  return list.length && list[0].value;
}