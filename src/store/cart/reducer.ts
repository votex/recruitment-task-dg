
import {
  CART_REMOVE_ITEM,
  CART_ADD_ITEM,
} from './types';
import { AnyAction } from 'redux';
import { ItemInterface } from '../../api/items';

export type CartStateInterface = Record<string, CartItemInterface[]>

const initialState: CartStateInterface = {
  cart: [],
}

export interface CartItemInterface extends ItemInterface {
  quantity: number,
  value: number,
}

export const cartReducer = (state = initialState, action: AnyAction) => {
  switch (action.type) {
    case CART_ADD_ITEM: {
      const { cart } = state;
      const existingCartItem: CartItemInterface = cart.find(cartItem => cartItem.id === action.payload.item.id);
      if (!cart.length || !existingCartItem) {
        return {
          cart: [
            ...cart,
            {
              ...action.payload.item,
              quantity: 1,
              value: action.payload.item.price,
            }
          ]
        }
      } else {
        const indefOfExistingItem = cart.findIndex(cartItem => cartItem.id === action.payload.item.id);
        cart[indefOfExistingItem].quantity++;
        cart[indefOfExistingItem].value = parseFloat((cart[indefOfExistingItem].quantity * action.payload.item.price).toFixed(2));
        return {
          cart: [...cart],
        }
      }
    }
    case CART_REMOVE_ITEM: {
      const { cart } = state;
      const existingCartItem = cart.find(cartItem => cartItem.id === action.payload.cartItem.id);
      if(existingCartItem) {
        const indefOfExistingItem = cart.findIndex(cartItem => cartItem.id === action.payload.cartItem.id);
        if (existingCartItem.quantity >= 2) {
          cart[indefOfExistingItem].quantity--;
          cart[indefOfExistingItem].value = parseFloat((cart[indefOfExistingItem].quantity * action.payload.cartItem.price).toFixed(2));
          return { cart: [...cart] }
        } else {
          return {
            cart: [...cart.filter(cartItem => cartItem.id !== action.payload.cartItem.id)]
          }
        }
      }
      break;
    }
    default:
      return state;
  }
}