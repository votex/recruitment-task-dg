import { AnyAction } from 'redux';
import {
  CART_ADD_ITEM,
  CART_REMOVE_ITEM,
} from './types';
import { ItemInterface } from '../../api/items';
import { CartItemInterface } from './reducer';

export interface AddItemToCartAction extends AnyAction {
  payload: { item: ItemInterface }
}

export interface RemoveItemFromCartAction extends AnyAction {
  payload: { cartItem: CartItemInterface }
}

export const cartActions = {
  addItemToCart: (item: ItemInterface): AddItemToCartAction => ({
    type: CART_ADD_ITEM,
    payload: { item },
  }),
  removeItemFromCart: (cartItem: CartItemInterface): RemoveItemFromCartAction => ({
    type: CART_REMOVE_ITEM,
    payload: { cartItem },
  })
}