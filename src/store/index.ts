import { createStore, combineReducers } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import { CartStateInterface, cartReducer } from './cart/reducer';

export interface ApplicationState {
  cartReducer: CartStateInterface;
}

const reducers = combineReducers<ApplicationState>({
  cartReducer,
})

export const store = createStore(reducers, {}, composeWithDevTools())
