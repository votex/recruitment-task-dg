export interface ItemInterface {
  id: number;
  title: string;
  cover: string;
  availability: boolean;
  price: number | null;
  currency: string;
}

export type ItemsStateInterface = Record<string, ItemInterface[]>
