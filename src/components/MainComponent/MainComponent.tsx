import * as React from "react";
import ProductsComponent from '../ProductsComponent/ProductComponent';
import CartComponent from "../CartComponent/CartComponent";

export const MainComponent: React.FC = (): JSX.Element => {
  
  return <div className="mainComponent grid">
    <ProductsComponent />
    <CartComponent />
  </div>;
}
