import * as React from "react";

type Props = {
  summaryCartValue: number,
};

export const CartSummary = ({ summaryCartValue }: Props): JSX.Element => {
  
  return <div
      className="cartSummary"
    >
      <p>Total:</p>
      {summaryCartValue}
      <p>PLN</p>
  </div>;
}
