import * as React from "react";
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { cartActions, RemoveItemFromCartAction } from '../../store/cart/actions';
import { CartItemInterface } from "../../store/cart/reducer";
import { CartItem } from './CartItem/CartItem';
import { cartItemsStateSelector, cartSummaryItemsValueSelector } from '../../store/cart/selectors';
import { ApplicationState } from "../../store";
import { CartSummary } from "./CartSummary/CartSummary";

type Props = {
  cartItems: CartItemInterface[];
  summary: number;
  removeCartItem: (cartItem: CartItemInterface) => RemoveItemFromCartAction,
}

const CartComponent: React.FC<Props> = ({ cartItems, removeCartItem, summary }: Props): JSX.Element => {
  const removeItemCallback = React.useCallback((cartItem: CartItemInterface) => {
    removeCartItem(cartItem);
  },                                           [removeCartItem]);

  const generateCartItems = React.useCallback(() => {
    return cartItems.length
    ? cartItems.map((item: CartItemInterface, i: number) => {
      return (
        <CartItem
          key={i}
          dataCartItem={item}
          removeCartItem={removeItemCallback}
        />
      )
    })
    : <p className="cartComponent__additional-info">Cart is empty</p>;
  },                                           [cartItems, removeItemCallback])

  return  <div className="grid cartComponent">
      <h2 className="cartComponent__title">Cart</h2>
      {generateCartItems()}
      {!!cartItems.length &&
        <CartSummary summaryCartValue={summary} />
      }
    </div>
}

const mapStateToProps = (state: ApplicationState): Partial<Props> => ({
  cartItems: cartItemsStateSelector(state),
  summary: cartSummaryItemsValueSelector(state),
});

const mapDispatchToProps = (dispatch: Dispatch): Partial<Props> => ({
  removeCartItem: (cartItem: CartItemInterface): RemoveItemFromCartAction => dispatch(cartActions.removeItemFromCart(cartItem)),
});

export default connect(mapStateToProps, mapDispatchToProps)(CartComponent);
