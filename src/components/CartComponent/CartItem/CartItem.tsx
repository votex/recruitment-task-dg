import * as React from "react";
import { CartItemInterface } from "../../../store/cart/reducer";

type Props = {
  dataCartItem: CartItemInterface,
  removeCartItem: (item: CartItemInterface) => void
};

export const CartItem = ({ dataCartItem, removeCartItem }: Props): JSX.Element => {
  
  return <div
      className=" grid cardItem"
    >
    <img className="cardItem__cover" src={dataCartItem.cover} alt={dataCartItem.title} />
    <div className="cardItem__content-container">
      <h4>{dataCartItem.title}</h4>
      <p>{dataCartItem.quantity}</p>
      <p>{dataCartItem.price}</p>
      <p>{dataCartItem.value}</p>
      <button
        onClick={() => removeCartItem(dataCartItem)}
      >
        X
      </button>
    </div>
  </div>;
}
