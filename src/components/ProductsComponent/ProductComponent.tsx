import * as React from "react";
import { Dispatch } from 'redux';
import { connect } from 'react-redux';
import { cartActions, AddItemToCartAction } from '../../store/cart/actions';
import { ItemInterface } from '../../api/items';
import { items } from '../../api/items/mockItems';
import { ProductItem } from './ProductItem/ProductItem';

type Props = {
  items: ItemInterface[]
  addItem: (item: ItemInterface) => AddItemToCartAction,
}

const ProductsComponent: React.FC<Props> = ({ items, addItem }: Props): JSX.Element => {
  const addItemCallback = React.useCallback((item: ItemInterface) => {
    addItem(item);
  },                                         [addItem]);

  const generateItems = React.useCallback(() => {
    return items && items.map((item: ItemInterface, i: number) => {
      return (
        <ProductItem
          key={i}
          dataItem={item}
          addItemToCart={addItemCallback}
        />
      )
    });
  },                           [items, addItemCallback]);
  
  return  <div className="grid productsComponent">
      <h2 className="productsComponent__title">Products</h2>
      {generateItems()}
    </div>
}


const mapStateToProps = (): Partial<Props> => ({
  items: items,
});

const mapDispatchToProps = (dispatch: Dispatch): Partial<Props> => ({
  addItem: (item: ItemInterface): AddItemToCartAction => dispatch(cartActions.addItemToCart(item)),
});

export default connect(mapStateToProps, mapDispatchToProps)(ProductsComponent);
