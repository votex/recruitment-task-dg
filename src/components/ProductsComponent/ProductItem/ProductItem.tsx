import * as React from "react";
import { ItemInterface } from "../../../api/items";

type Props = {
  dataItem: ItemInterface,
  addItemToCart: (item: ItemInterface) => void
};

export const ProductItem: React.FC<Props> = ({ dataItem, addItemToCart }: Props): JSX.Element | null => {
  return dataItem.availability
    ? <div
        style={{ background: `${dataItem.cover}` }}
        className="productItem"
      >
      <img src={dataItem.cover} alt={dataItem.title} />
      <h4>{dataItem.title}</h4>
      <p>{dataItem.price}</p>
      <button
        onClick={() => addItemToCart(dataItem)}
      >
        Add to cart
      </button>
    </div>
    : null;
}
